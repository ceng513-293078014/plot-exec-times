import json
import sys
import matplotlib.pyplot as plt

def load_results(file_name):
    with open(file_name, "r") as f:
        return json.load(f)

def get_exec_times(
  resultsgcc,
  resultsclang
):
  arr_gcc = resultsgcc["tests"]
  arr_clang = resultsclang["tests"]

  test_names = []
  exec_times_gcc = []
  exec_times_clang = []

  for result_g in arr_gcc:

    test_name = result_g['name'].rsplit(':', 1)[-1]

    for result_c in arr_clang:

      if test_name != result_c['name'].rsplit(':', 1)[-1] : continue

      if 'metrics' in result_g and 'exec_time' in result_g['metrics']:
        test_names.append(test_name)
        exec_times_gcc.append(result_g['metrics']['exec_time'])

      if 'metrics' in result_c and 'exec_time' in result_c['metrics']:
        exec_times_clang.append(result_c['metrics']['exec_time'])

  return test_names, exec_times_gcc, exec_times_clang


def plot(
  test_names,
  exec_times_gcc,
  exec_times_clang
):
  fig, ax = plt.subplots(figsize=(10, 7))
  ax.set_ylabel("Execution Time")
  ax.set_title(f"Comparison of Execution Times")
  ax.set_xticklabels(test_names, rotation=45,  ha="right", fontsize=7)

  fig.tight_layout()
  plt.scatter(test_names, exec_times_gcc, label='gcc values', c='blue')
  plt.scatter(test_names, exec_times_clang, label='clang values', c='red')
  plt.legend(loc='upper right')
  plt.savefig("interpolation.png")

if __name__ == "__main__":

  resultsgcc = load_results(f"test-suite-build-gcc/results.json")
  resultsclang = load_results(f"test-suite-build-clang/results.json")

  (
    test_names,
    exec_times_gcc,
    exec_times_clang
  ) = get_exec_times(resultsgcc, resultsclang)

  plot(test_names, exec_times_gcc, exec_times_clang)

  